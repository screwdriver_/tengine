#include "tengine.hpp"

using namespace TEngine;

string TEngine::make_safe(string path)
{
	if(path.empty()) path = ".";
	if(path.back() != '/') path += '/';

	return path;
}

int TEngine::open_stream(const string& path)
{
	errno = 0;
	const auto fd = open(path.c_str(), O_CREAT | O_RDWR,
		S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);

	if(errno != 0) {
		errno = 0;
		throw runtime_error("Failed to open stream!");
	}

	return fd;
}

void TEngine::remove_directory(string path)
{
	if(path.back() != '/') path += '/';

	DIR* dir;

	if(!(dir = opendir(path.c_str()))) {
		throw runtime_error("Failed to open directory!");
	}

	dirent* entry;

	while((entry = readdir(dir))) {
		const string name(entry->d_name);
		if(name == "." || name == "..") continue;

		const auto& type = entry->d_type;

		if(type == DT_DIR) {
			remove_directory(path + name);
		}

		remove((path + name).c_str());
	}

	closedir(dir);
	remove(path.c_str());
}

string TEngine::get_type(const Type type)
{
	return types[type];
}

Type TEngine::get_type(const string& str)
{
	uint8_t type = INT_T;
	size_t i = 0;

	while(i < str.size() && type <= CLOB_T) {
		if(i >= strlen(types[type])
			|| types[type][i] != str[i]) {
			++type;
		} else {
			++i;
		}
	}

	return (Type) type;
}

uint8_t TEngine::get_default_size(const Type type)
{
	switch(type) {
		case INT_T: {
			return 4;
		}

		case FLOAT_T: {
			return 4;
		}

		case BOOL_T: {
			return 1;
		}

		case STRING_T: {
			return 32;
		}

		case CLOB_T: {
			return sizeof(clob_pos_t);
		}

		default: {
			return 0;
		}
	}
}

// TODO Re-write this function
bool TEngine::is_valid_columnName(const string& name)
{
	if(name.empty()) return false;
	return ((name.front() >= 'a' && name.front() <= 'z')
		|| (name.front() >= 'A' && name.front() <= 'Z')
		|| name.front() == '_');
}
