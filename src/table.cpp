#include "tengine.hpp"

using namespace TEngine;

Table::Table(const string& tables_path, const string& name)
	: path{make_safe(tables_path) + name + '/'}, name{name}
{
	if(is_new()) {
		prepare();
	} else {
		load();
	}

	precalculate();
	open_streams();
}

bool Table::is_new() const
{
	DIR* dir;
	const bool exists = ((dir = opendir(path.c_str())) != nullptr);

	closedir(dir);
	return !exists;
}

void Table::prepare()
{
	mkdir(path.c_str(), 0777);
	mkdir((path + "indexes/").c_str(), 0777);

	next_index = BEGIN_INDEX;

	open_head();
	write_head();
	WAL_init();
}

void Table::load()
{
	open_head();
	read_head();
	WAL_init();
}

void Table::open_head()
{
	const string head_path(path + "head");
	head_fd = open_stream(head_path);
}

void Table::read_head()
{
	const size_t size = lseek(head_fd, 0, SEEK_END);
	char* buffer;

	if(!(buffer = (char*) malloc(size))) {
		throw bad_alloc();
	}

	pread(head_fd, buffer, size, 0);

	size_t i = 0;

	memcpy(&next_index, &buffer[i], sizeof(next_index));
	i += sizeof(next_index);

	while(i < size) {
		const string name(&buffer[i]);
		i += name.size() + 1;

		Type type;
		memcpy(&type, &buffer[i], sizeof(type));
		i += sizeof(type);

		uint8_t size;
		memcpy(&size, &buffer[i], sizeof(size));
		i += sizeof(type);

		columns.emplace_back(name, type, size);
	}

	free(buffer);
}

void Table::write_head()
{
	size_t size = sizeof(next_index);

	for(const auto& c : columns) {
		size += c.get_head_size();
	}

	string buffer;
	buffer.reserve(size);
	size_t i = 0;

	memcpy(&buffer[i], &next_index, sizeof(next_index));
	i += sizeof(next_index);

	for(const auto& c : columns) {
		const auto& name = c.get_name();
		memcpy(&buffer[i], name.c_str(), name.size() + 1);
		i += name.size() + 1;

		const auto type = c.get_type();
		memcpy(&buffer[i], &type, sizeof(type));
		i += sizeof(type);

		const auto size = c.get_size();
		memcpy(&buffer[i], &size, sizeof(size));
		i += sizeof(size);
	}

	pwrite(head_fd, buffer.c_str(), size, 0);
	ftruncate(head_fd, size);
}

void Table::precalculate()
{
	cols_begins.reserve(columns.size());

	size_t pos = 0;

	for(size_t i = 0; i < columns.size(); ++i) {
		cols_begins[i] = pos;
		pos += columns[i].get_size();
	}

	row_size = pos;
}

void Table::open_streams()
{
	const string data_path(path + "data.bin");
	stream_fd = open_stream(data_path);

	const string clobs_path(path + "clobs.data");
	clobs_stream_fd = open_stream(clobs_path);

	update_data_size();
}

void Table::update_data_size()
{
	data_size = lseek(stream_fd, 0, SEEK_END);
	errno = 0;
}

void Table::WAL_init()
{
	const string wal_path(path + "write_ahead.log");
	wal_fd = open_stream(wal_path);

	if((wal_size = lseek(wal_fd, 0, SEEK_END)) == 0) {
		WAL_reset();
		return;
	}

	pread(wal_fd, (char*) &wal_state, sizeof(wal_state), 0);

	switch(wal_state) {
		case PREPROCESSING: {
			WAL_reset();
			break;
		}

		case PROCESSING: {
			data_flush();
			break;
		}

		default: {
			break;
		}
	}
}

void Table::WAL_reset()
{
	WAL_set_state(READY);

	ftruncate(wal_fd, sizeof(wal_state));
	wal_size = sizeof(wal_state);
}

void Table::WAL_set_state(const WALState state)
{
	wal_state = state;
	pwrite(wal_fd, (const char*) &wal_state, sizeof(wal_state), 0);
}

void Table::WAL_write_op(const WALOperation op)
{
	pwrite(wal_fd, &op, sizeof(op), wal_size);
	wal_size += sizeof(op);
}

void Table::data_write(const char* buffer, const uint64_t size,
	const uint64_t pos)
{
	WAL_set_state(PREPROCESSING);

	WAL_write_op(WRITE);

	pwrite(wal_fd, (const char*) &pos, sizeof(pos), wal_size);
	wal_size += sizeof(pos);

	pwrite(wal_fd, (const char*) &size, sizeof(size), wal_size);
	wal_size += sizeof(size);

	pwrite(wal_fd, buffer, size, wal_size);
	wal_size += size;
}

void Table::data_truncate(const uint64_t size)
{
	WAL_set_state(PREPROCESSING);

	WAL_write_op(TRUNCATE);

	pwrite(wal_fd, (const char*) &size, sizeof(size), wal_size);
	wal_size += sizeof(size);
}

void Table::data_flush()
{
	WAL_set_state(PROCESSING);

	for(size_t i = sizeof(WALState); i < wal_size;) {
		WALOperation op;
		pread(wal_fd, (char*) &op, sizeof(op), i);
		i += sizeof(op);

		switch(op) {
			case WRITE: {
				uint64_t pos;
				pread(wal_fd, (char*) &pos, sizeof(pos), i);
				i += sizeof(pos);

				uint64_t size;
				pread(wal_fd, (char*) &size, sizeof(size), i);
				i += sizeof(size);

				if(size == 0) break;

				char* buffer;

				if(!(buffer = (char*) malloc(size))) {
					throw bad_alloc();
				}

				pread(wal_fd, buffer, size, i);
				i += size;

				pwrite(stream_fd, buffer, size, pos);

				free(buffer);
				break;
			}

			case TRUNCATE: {
				uint64_t size;
				pread(wal_fd, (char*) &size, sizeof(size), i);
				i += sizeof(size);

				ftruncate(stream_fd, size);
				break;
			}
		}
	}

	WAL_reset();
}

const Column* Table::get_column(const size_t index) const
{
	return (index < columns.size() ? &columns[index] : nullptr);
}

const Column* Table::get_column(const string& name) const
{
	for(const auto& c : columns) {
		if(c.get_name() == name) return &c;
	}

	return nullptr;
}

void Table::set_column(const Column& column)
{
	for(size_t i = 0; i < columns.size(); ++i) {
		auto& c = columns[i];

		if(c.get_name() == column.get_name()) {
			alter_resize(cols_begins[i], c.get_size(), column.get_size());
			c = column;
			write_head();
			precalculate();

			return;
		}
	}

	alter_add(row_size, column.get_size());
	columns.push_back(column);
	write_head();
	precalculate();
}

void Table::set_column(const size_t index, const Column& column)
{
	alter_resize(cols_begins[index], columns[index].get_size(), column.get_size());
	columns[index] = column;
	write_head();
	precalculate();
}

void Table::remove_column(const size_t index)
{
	if(index >= columns.size()) throw invalid_argument("Column not found!");

	alter_remove(cols_begins[index], columns[index].get_size());
	columns.erase(columns.begin() + index);
	write_head();
	precalculate();
}

void Table::remove_column(const string& name)
{
	for(size_t i = 0; i < columns.size(); ++i) {
		const auto& c = columns[i];

		if(c.get_name() == name) {
			alter_remove(cols_begins[i], c.get_size());
			columns.erase(columns.begin() + i);
			write_head();
			precalculate();

			return;
		}
	}

	throw invalid_argument("Column not found!");
}

void Table::alter_add(const size_t pos, const uint8_t size)
{
	const auto rows = get_rows_count();
	char* buffer;

	if(!(buffer = (char*) malloc(size))) {
		throw bad_alloc();
	}

	const char* empty_buffer;

	if(!(empty_buffer = (const char*) calloc(size, 1))) {
		throw bad_alloc();
	}

	for(auto i = rows; i >= 1; --i) {
		const auto read_pos = (i - 1) * row_size + pos;
		auto read_size = row_size;

		if(read_pos + read_size > data_size) {
			read_size = data_size - read_pos;
		}

		pread(stream_fd, buffer, read_size, read_pos);
		data_write(buffer, read_size,
			(i - 1) * (row_size + size) + pos + size);
		data_write(empty_buffer, size,
			(i - 1) * (row_size + size) + pos);
	}

	free((void*) buffer);
	free((void*) empty_buffer);

	data_flush();

	errno = 0;
	update_data_size();
}

void Table::alter_resize(const size_t pos,
	const uint8_t from, const uint8_t to)
{
	if(from == to) return;

	if(from > to) {
		alter_remove(pos, from - to);
	} else {
		alter_add(pos, to - from);
	}
}

void Table::alter_remove(const size_t pos, const uint8_t size)
{
	const auto rows = get_rows_count();
	const auto new_size = row_size - size;
	char* buffer;

	if(!(buffer = (char*) malloc(new_size))) {
		throw bad_alloc();
	}

	for(size_t i = 0; i < rows; ++i) {
		pread(stream_fd, buffer, new_size, i * row_size + pos + size);
		data_write(buffer, new_size, i * new_size + pos);
	}

	free((void*) buffer);

	data_truncate(rows * new_size);
	data_flush();

	errno = 0;
	update_data_size();
}

size_t Table::get_column_begin(const size_t index) const
{
	if(index >= cols_begins.size()) throw invalid_argument("Column not found!");
	return cols_begins[index];
}

size_t Table::get_column_begin(const string& column) const
{
	for(size_t i = 0; i < columns.size(); ++i) {
		if(columns[i].get_name() == column) {
			return cols_begins[i];
		}
	}

	throw invalid_argument("Column not found!");
}

Row Table::get_row(const index_t index)
{
	Row row(*this, index);
	pread(stream_fd, row.data, row_size, get_row_position(index));
	errno = 0;

	return row;
}

void Table::get_rows(const index_t begin, const index_t end,
	const function<void(const Row& row)>& handle)
{
	if(end >= begin) return;

	const size_t buff_size = (row_size < DEFAULT_BUFFER_SIZE
		? DEFAULT_BUFFER_SIZE : row_size);
	char* buffer;

	if(!(buffer = (char*) malloc(buff_size))) {
		throw bad_alloc();
	}

	const size_t rows_per_buffer = buff_size / row_size;
	size_t i = begin;

	while(i < end) {
		pread(stream_fd, buffer, row_size, get_row_position(i));

		size_t j = 0;

		while(i < end && j < rows_per_buffer) {
			const auto p = buffer + (i % rows_per_buffer * row_size);

			Row row(*this, i);
			memcpy(row.data, p, row_size);
			handle(row);

			++i;
			++j;
		}
	}

	free(buffer);
}

void Table::get_rows(Condition& condition,
	const function<void(const Row& row)>& handle)
{
	// TODO Disk reading blocks optimization
	// TODO Indexes optimization

	for(size_t i = 0; i < get_rows_count(); ++i) {
		const auto row = get_row(i + 1);

		if(condition.check_row(row, *this)) {
			handle(row);
		}
	}
}

void Table::set_row(const Row& row)
{
	data_write(row.data, row_size, get_row_position(row.index));
	data_flush();

	errno = 0;
	update_data_size();
}

void Table::set_rows(Condition& condition, const Row& row)
{
	// TODO Disk reading blocks optimization
	// TODO Indexes optimization

	for(size_t i = 0; i < get_rows_count(); ++i) {
		const auto r = get_row(i + 1);

		if(condition.check_row(r, *this)) {
			data_write(row.data, row_size, get_row_position(i + 1));
		}
	}

	data_flush();

	errno = 0;
	update_data_size();
}

void Table::insert_row(Row& row)
{
	row.index = next_index;
	set_row(row);

	++next_index;
	write_next_index();
}

void Table::write_next_index() const
{
	// TODO WAL
	pwrite(head_fd, (const char*) &next_index, sizeof(next_index), 0);
}

void Table::remove_row(const index_t index)
{
	const char* buffer;

	if(!(buffer = (const char*) calloc(row_size, 1))) {
		throw bad_alloc();
	}

	data_write(buffer, row_size, get_row_position(index));
	data_flush();

	errno = 0;
	update_data_size();
}

void Table::remove_rows(const index_t begin, const index_t end)
{
	if(end >= begin) return;

	// TODO Optimization
	const auto size = (end - begin) * row_size;
	const char* buffer;

	if(!(buffer = (const char*) calloc(size, 1))) {
		throw bad_alloc();
	}

	data_write(buffer, size, begin * row_size);
	data_flush();

	errno = 0;
	update_data_size();
}

void Table::remove_rows(Condition& condition)
{
	// TODO Disk reading blocks optimization
	// TODO Indexes optimization

	const char* buffer;

	if(!(buffer = (const char*) calloc(row_size, 1))) {
		throw bad_alloc();
	}

	for(size_t i = 0; i < get_rows_count(); ++i) {
		const auto row = get_row(i);

		if(condition.check_row(row, *this)) {
			data_write(buffer, row_size, i * row_size);
		}
	}

	data_flush();

	errno = 0;
	update_data_size();
}

char* Table::read_clob(const clob_pos_t pos) const
{
	size_t size;
	pread(clobs_stream_fd, (char*) &size, sizeof(size), pos);

	char* buffer;

	if(!(buffer = (char*) malloc(size))) {
		throw bad_alloc();
	}

	pread(clobs_stream_fd, buffer, size, pos + sizeof(size));

	errno = 0;
	return buffer;
}

void Table::write_clob(clob_pos_t& pos, const char* data) const
{
	size_t size;
	pread(clobs_stream_fd, (char*) &size, sizeof(size), pos);

	const auto data_size = strlen(data);

	if(data_size > size) {
		pos = lseek(stream_fd, 0, SEEK_END);
	}

	// TODO WAL
	pwrite(clobs_stream_fd, (char*) &data_size, sizeof(data_size), pos);
	pwrite(clobs_stream_fd, data, data_size, pos + sizeof(size));
	errno = 0;
}

void Table::defrag_clobs() const
{
	// TODO
	errno = 0;
}

void Table::clear()
{
	data_truncate(0);
	data_flush();

	// TODO Remove CLOBs

	next_index = BEGIN_INDEX;
	write_next_index();

	update_data_size();
}

void Table::remove_table() const
{
	close(head_fd);
	close(stream_fd);
	close(clobs_stream_fd);
	remove_directory(get_path());

	errno = 0;
}
