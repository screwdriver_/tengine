#ifndef TENGINE_HPP
# define TENGINE_HPP

# include<cstring>
# include<functional>
# include<fstream>
# include<iostream>
# include<memory>
# include<stdexcept>
# include<string>
# include<vector>

# include<dirent.h>
# include<errno.h>
# include<fcntl.h>
# include<unistd.h>
# include<sys/stat.h>
# include<sys/types.h>

# include "condition/condition.hpp"

namespace TEngine
{
	using namespace std;

	typedef uint8_t tree_factor_t;
	typedef uint32_t index_t;
	typedef uint32_t clob_pos_t;

	constexpr index_t BEGIN_INDEX = 1;
	constexpr size_t DEFAULT_BUFFER_SIZE = 1048576;

	string make_safe(string path);
	int open_stream(const string& path);
	void remove_directory(string path);

	enum Type : uint8_t
	{
		INT_T = 0,
		FLOAT_T,
		BOOL_T,
		STRING_T,
		CLOB_T
	};

	const vector<const char*> types = {
		"INT", "FLOAT", "BOOL", "STRING", "CLOB"
	};

	string get_type(const Type type);
	Type get_type(const string& str);
	uint8_t get_default_size(const Type type);
	bool is_valid_columnName(const string& name);

	// TODO Handle minimum and fix sizes
	class Column
	{
		public:
			inline Column(const string& name, const Type type, const uint8_t size)
				: name{name}, type{type}, size{size}
			{}

			inline Column(const string& name, const Type type)
				: name{name}, type{type}, size{get_default_size(type)}
			{}

			inline const string& get_name() const
			{
				return name;
			}

			inline Type get_type() const
			{
				return type;
			}

			inline uint8_t get_size() const
			{
				return size;
			}

			inline size_t get_head_size() const
			{
				return name.size() + 1 + sizeof(type) + sizeof(size);
			}

			inline bool can_be_indexed()
			{
				return (type != CLOB_T);
			}

		private:
			string name;
			Type type;
			uint8_t size;
	};

	template<typename T>
	class TreeNode
	{
		public:
			TreeNode(const string& path);

			inline tree_factor_t get_factor() const
			{
				return factor;
			}

			char* get_values() const;
			// TODO

		protected:
			inline TreeNode(const int fd, const ssize_t pos,
				const tree_factor_t factor)
				: fd{fd}, pos{pos}, factor{factor}
			{}

		private:
			const int fd;
			const ssize_t pos;
			const tree_factor_t factor;
	};

	enum WALState : uint8_t
	{
		READY = 0,
		PREPROCESSING = 1,
		PROCESSING = 2
	};

	enum WALOperation : uint8_t
	{
		WRITE = 0,
		TRUNCATE = 1
	};

	class Row;

	class Table
	{
		public:
			Table(const string& tables_path, const string& name);

			inline const string& get_name() const
			{
				return name;
			}

			inline string get_path() const
			{
				return path;
			}

			inline const vector<Column>& get_columns() const
			{
				return columns;
			}

			const Column* get_column(const size_t index) const;
			const Column* get_column(const string& name) const;
			void set_column(const Column& column);
			void set_column(const size_t index, const Column& column);
			void remove_column(const size_t index);
			void remove_column(const string& name);

			size_t get_column_begin(const size_t index) const;
			size_t get_column_begin(const string& column) const;

			inline size_t get_column_begin(const Column& column) const
			{
				return get_column_begin(column.get_name());
			}

			inline size_t get_row_size() const
			{
				return row_size;
			}

			inline size_t get_data_size() const
			{
				return data_size;
			}

			inline index_t get_rows_count() const
			{
				return (row_size != 0 ? data_size / row_size : 0);
			}

			inline index_t get_next_index() const
			{
				return next_index;
			}

			inline uint64_t get_row_position(const index_t index) const
			{
				if(index == 0) throw invalid_argument("Out of range!");
				return (index - BEGIN_INDEX) * row_size;
			}

			Row get_row(const index_t index);
			void get_rows(const index_t begin, const index_t end,
					const function<void(const Row& row)>& handle);
			void get_rows(Condition& condition,
					const function<void(const Row& row)>& handle);

			void set_row(const Row& row);
			void set_rows(Condition& condition, const Row& row);

			void insert_row(Row& row);

			void remove_row(const index_t index);
			void remove_rows(const index_t begin, const index_t end);
			void remove_rows(Condition& condition);

			char* read_clob(const clob_pos_t pos) const;
			void write_clob(clob_pos_t& pos, const char* data) const;

			void clear();

			void remove_table() const;

		private:
			string path;
			string name;

			vector<Column> columns;

			vector<size_t> cols_begins;
			size_t row_size;

			int head_fd;
			int wal_fd;
			int stream_fd;
			int clobs_stream_fd;

			WALState wal_state;
			size_t wal_size;

			size_t data_size;
			index_t next_index;

			bool is_new() const;
			void prepare();
			void load();

			void open_head();

			void read_head();
			void write_head();

			void precalculate();
			void open_streams();
			void update_data_size();

			void WAL_init();
			void WAL_reset();
			void WAL_set_state(const WALState state);
			void WAL_write_op(const WALOperation op);

			void data_write(const char* buffer, const uint64_t size,
				const uint64_t pos);
			void data_truncate(const uint64_t size);
			void data_flush();

			void alter_add(const size_t pos, const uint8_t size);
			void alter_resize(const size_t pos,
					const uint8_t from, const uint8_t to);
			void alter_remove(const size_t pos, const uint8_t size);

			void write_next_index() const;

			void defrag_clobs() const;
	};

	class Row
	{
		public:
			index_t index;
			char* data;

			inline Row(Table& table)
				: data{(char*) malloc(table.get_row_size())},
				table{&table}
			{}

			inline Row(Table& table, const index_t index)
				: index{index}, data{(char*) malloc(table.get_row_size())},
				table{&table}
			{}

			inline ~Row()
			{
				free(data);
			}

			template<typename T>
			T get(const Column& column) const;

			template<typename T>
			inline T get(const string& column) const
			{
				const auto c = table->get_column(column);
				if(!c) throw invalid_argument("Column not found!");

				return get<T>(*c);
			}

			string get_clob(const Column& column) const;

			inline string get_clob(const string& column) const
			{
				const auto c = table->get_column(column);
				if(!c) throw invalid_argument("Column not found!");

				return get_clob(*c);
			}

			template<typename T>
			void set(const Column& column, const T& value);

			template<typename T>
			inline void set(const string& column, const T& value)
			{
				const auto c = table->get_column(column);
				if(!c) throw invalid_argument("Column not found!");

				set<T>(*c, value);
			}

			void set_clob(const Column& column, const string& value);

			inline void set_clob(const string& column, const string& value)
			{
				const auto c = table->get_column(column);
				if(!c) throw invalid_argument("Column not found!");

				set_clob(*c, value);
			}

			inline void remove()
			{
				table->remove_row(index);
			}

		private:
			Table* table;
	};
}

#include "row.tpp"

#endif
