# TEngine

TEngine is the engine for the TDb database server.
This engine stores in predefined tables.



## Overview

The aim of this engine is to store and retrieve data into tables as fast as possible.
To achieve this goal, the engine follows this strategy:

The user of the database must define tables and the columns of this table.

Each column has the following characteristics:
- **Position**: The position of the columns in the tables
- **Name**: A name allowing to identify the column
- **Type**: The type of the data that can be contained in the column
- **Size**: The size (in bytes) of the data in the column

Data types are the following:

| Name   | Default size |
|--------|--------------|
| INT    | 4            |
| FLOAT  | 4            |
| BOOL   | 1            |
| STRING | 32           |
| CLOB   | n/a          |



A column's size can't exceed **255** bytes.
In order to store larger pieces of data, the **CLOB** type must be used.

A **CLOB** (Char Long OBject) allows to store large data inside of a table.
The data which is actually stored into the table is a **4** bytes long integer which points to the data in another storage location.



Every transaction performed by the engine is **ACID**:
- **Atomicity**: Each transaction is performed as a single unit, which either succeeds completly or fails completly. This is achieved using **Write Ahead Log**.
- **Consistency**: A transaction can't corrupt the database.
- **Isolation**: Concurrency of transactions execution doesn't affect the final state of the database.
- **Durability**: When a transaction succeeds, it remains succeeded even in case of crash.



Every line is paired to a **4** bytes long integer which is its **id**.
Line ids, are used to retrieve a line's position in **O(1)** time, but also to identify lines inside of indexes.



## Head file

The head file of tables stores metadata about the table.

This file contains the following informations:
- **Next id** (4 bytes): The id for the next line to be inserted into the table
- **Column**: A pattern repeated for each column
	- **Name**: The column's name ended by ``\0``
	- **Type** (1 byte): The column type id
	- **Size** (1 byte): The column's size



## Write Ahead Log

Write AHead Log ensures that transactions are **ACID**.
This process has several states:
- **READY**: No transaction is currently being treated
- **PREPROCESSING**: The engine is writing operations in logs
- **PROCESSING**: The engine is performing operations



Before processing a write operation on a table, the engine writes the all operations to perform in the log.

Here are the available operations:
- **WRITE**: Writes data to the database
- **TRUNCATE**: Truncates the database



If the log is in **PREPROCESSING** state at startup, it means that the database crashed during writing operations in logs. Thus, the database can't complete the transaction and cancels it.

If the log is in **PROCESSING** state at startup, it means that the database crashed during database writing. Since operations are written in the log, the transaction can be finished.

When a transaction is finished, the engine clears the log and goes back to the **READY** state.



## Indexes

An index is an object allowing to reduce the complexity of lines searching. These objects are based on B+ Trees, which allows to search data in **O(log(n))** time.

An index can be created for each column of each table. Then when the engine will look for lines that have a specific value in an indexed column, it will use the index to achieve **O(log(n))** search intead of **O(n)**.

When the table is modified, indexes are updated.

TODO
